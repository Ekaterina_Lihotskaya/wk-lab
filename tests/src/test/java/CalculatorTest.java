
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorTest {
    private int a = 10;
    private int b = 5;
    private int result;

    @Test
    void sum() {
        result = 15;
        assertThat(result, equalTo(Calculator.sum(a, b)));
    }

    @Test
    void difference() {
        result = 5;
        assertThat(result, equalTo(Calculator.difference(a, b)));
    }

    @Test
    void division() {
        float result = 2f;
        assertThat(result, equalTo(Calculator.division(a, b)));
    }

    @Test
    void divisionIfArgumentBEqualsNull() {
        int b = 0;
        assertThrows(ArithmeticException.class,
                () -> {
                    Calculator.division(a, b);
                });
    }

    @Test
    void mult() {
        result = 50;
        assertThat(result, equalTo(Calculator.mult(a, b)));
    }

    @Test
    void sqrt() {
        double result = 1.58489;
        assertThat(result, closeTo(Calculator.sqrt(a, b), 0.00005));
    }

    @Test
    void sqrtIfArgumentBEqualsNull() {
        double result = 1f;
        int b = 0;
        assertThat(result, closeTo(Calculator.sqrt(a, b), 0.00005));
    }

    @Test
    void pow() {
        result = 100000;
        assertThat(result, equalTo(Calculator.pow(a, b)));

    }

    @Test
    void log() {
        List<Integer> initList = Arrays.asList(a, b);
        List<Float> result = Arrays.asList(2.3025851f, 1.609438f);
        assertThat(result, Matchers.contains(Calculator.log(initList).toArray()));
    }

    @Test
    void factorial() {
        result = 3628800;
        assertThat(result, equalTo(Calculator.factorial(a)));
    }

    @Test
    void factorialIfArgumentEqualNull() {
        result = 1;
        int a = 0;
        assertThat(result, equalTo(Calculator.factorial(a)));
    }
}