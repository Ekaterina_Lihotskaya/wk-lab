import java.util.ArrayList;
import java.util.List;

public class Calculator {

    public static int sum(int a, int b) {
        return a + b;
    }

    public static int difference(int a, int b) {
        return a - b;
    }

    public static float division(int a, int b) {
        if (b == 0) {
            throw new ArithmeticException("Enter right argument b");
        }
        return a / b;
    }

    public static int mult(int a, int b) {
        return a * b;
    }

    public static float sqrt(int a, int b) {
        if (b == 0) {
            return 1f;
        }
        return (float) Math.pow(a, 1f / b);
    }

    public static int pow(int a, int b) {
        return (int) Math.pow(a, b);
    }

    public static List<Float> log(List<Integer> list) {
        List<Float> resultList = new ArrayList<Float>();

        for (Integer element : list) {
            resultList.add((float) Math.log(element));
        }
        return resultList;
    }

    public static int factorial(int a) {
        int result = 1;

        for (int factor = 2; factor <= a; factor++) {
            result *= factor;
        }
        return result;
    }
}
