package com.epam;


import com.google.gson.Gson;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShowResult {
    public final static String PATTERN = "\\s*\\{\"name\"\\s*:\\s*\".*\"\\s*,\\s*\"surname\"\\s*:\\s*\".*\"\\s*,\\s*\"age\"\\s*:\\s*\\d+\\s*}\\s*";


    public static void checkParamrter(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("Path to file is upset.");
        }
    }

    public static Person getResult(String path) {
        String json = "";
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(path)))) {
            json = reader.readLine();

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("You path is not correct. Please write right path to file");
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (!json.matches(PATTERN)) {
            throw new IllegalArgumentException("Invalid JSON object");
        }
        Gson gson = new Gson();
        return gson.fromJson(json, Person.class);
    }
}

