package com.epam;

import lombok.Data;

@Data
public class Person {
    private String name;
    private String surname;
    private byte age;

    @Override
    public String toString() {
        return "Name: " + name +
                " SurName: " + surname +
                " Age: " + age;
    }
}
