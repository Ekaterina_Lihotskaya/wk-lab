# Maven 

Task description : Practical task - #Mvan.docx


## Install
Execute following command-line:
```
  cd <gson-parser>
  mvn clean install
```

## Usage
Execute following command-line:
```
java -jar gson-parser\gson-parser-boy\target\gson-parser-boy-1.0-SNAPSHOT-jar-with-dependencies.jar "path to file"
java -jar gson-parser\gson-parser-girl\target\gson-parser-girl-1.0-SNAPSHOT-jar-with-dependencies.jar "path to file"
```


